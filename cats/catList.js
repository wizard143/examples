document.addEventListener('DOMContentLoaded', function() {
	var menu = document.querySelector('div[name="menu"]');
	menu.innerHTML = 'Привет, ' + localStorage.getItem('name');
	var exit = document.createElement('button');
	exit.innerHTML = 'Выход';
	exit.name = 'exit';
	exit.onclick = function() {
		location.href = 'index.html';
		localStorage.removeItem('name');
	};

	var createHref = document.createElement('a');
	createHref.innerHTML = 'Создать котика';
	createHref.href = 'catCreate.html';

	var ent = document.createElement('br');

	menu.appendChild(exit);
	menu.appendChild(ent);
	menu.appendChild(createHref);
	
	//console.log(localStorage.getItem('name'));

	var content = document.querySelector('#content'), coorPrev = content.getBoundingClientRect().top;
	document.onscroll = function() {
		var diffScroll = coorPrev - content.getBoundingClientRect().top;
		if (coorPrev > content.getBoundingClientRect().top) {
			//console.log("down", content.getBoundingClientRect().top);

			if (parseInt(getComputedStyle(menu).top) < -parseInt(getComputedStyle(menu).height)) {
				menu.style.top = "-" + (parseInt(getComputedStyle(menu).height) + 11) + "px";
				console.error("if", getComputedStyle(menu).height, getComputedStyle(menu).top);		
			} else {
				menu.style.top = (parseInt(getComputedStyle(menu).top) - diffScroll) + "px";
				console.error("else");
			}

			//console.log(getComputedStyle(menu).top);
		} else if (coorPrev < content.getBoundingClientRect().top) {
			//console.log("up", getComputedStyle(menu).top, content.getBoundingClientRect().top, coorPrev, -(coorPrev - content.getBoundingClientRect().top));
			if (parseInt(getComputedStyle(menu).top) >= diffScroll) {
				menu.style.top = "0px";			
			}	else {
				menu.style.top = (parseInt(getComputedStyle(menu).top) - diffScroll) + "px";
				
			}

		}
		coorPrev = content.getBoundingClientRect().top;
	}


	var cats = [
		{
			img: 'img/1.jpg',
			name: 'Тося',
			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
			sex: 'Кошка',
			breed: 'Без породы',
			phone: '01234567899',
			email: 'test@test.ts'
		},
		{
			img: 'img/2.jpg',
			name: 'Кузя',
			breed: 'Норвежская',
			description: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
			sex: 'Кот',
			phone: '09987654321',
			email: ''
		}
	];
	cats = cats.concat(JSON.parse(localStorage.getItem('cats') || '[]'));
	var container = document.querySelector('.container');
	var dia = document.querySelector('dialog');
	
	cats.forEach(function(cat) {
		var catItem = document.createElement('div');
		catItem.classList.add('cat-item');
		catItem.innerHTML = `
			<img src="${cat.img}">
			<div class="cat-info">
				<h3>${cat.name}</h3>
				<p>${cat.description}</p>
				<p><i>Пол:
					<span>${cat.sex}</span>
				</i></p>
				<p><i>Порода:
					<span>${cat.breed}</span>
				</i></p>
				<address>Контакты:
					<span>${cat.phone} ${cat.email}</span>
				</address>
			</div>
		`;

		catItem.querySelector('img').onclick = function () {
			showCat(cat.img);
		};

		container.appendChild(catItem);
	});
	function showCat(img) {
		dia.querySelector('img').src = img;
		dia.showModal();
	}
	dia.onclick = function closeCat() {
		//console.log(dia.open);
		dia.close();
	}
});