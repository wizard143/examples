document.addEventListener('DOMContentLoaded', function() {
	var form = document.querySelector('form[name="create"]');
	var userName = document.querySelector('div[name="userName"]');
	userName.innerHTML = 'Привет, ' + localStorage.getItem('name');
	var exit = document.createElement('button');
	exit.innerHTML = 'Выход';
	exit.name = 'exit';
	exit.onclick = function() {
		location.href = 'index.html';
		localStorage.removeItem('name');
	};
	userName.appendChild(exit);
	var file = null;
	form.querySelector('input[type="file"]').onchange = function() {previewFile()};
	form.onsubmit = function() {
		//console.log(form);
		console.log("добавить");
		var name = form.querySelector('input[name="name"]').value;
		var sex = form.querySelector('input[name="sex"]').value;
		var description = form.querySelector('textarea').value;
		var breed = form.querySelector('select').value;
		var phone = form.querySelector('input[name="phone"]').value;
		var email = form.querySelector('input[name="email"]').value;
		//console.log(name, sex, file);
		var cats = JSON.parse(localStorage.getItem('cats') || '[]');
		//console.log(cats);
		cats.push({name, sex, breed, description, phone, email, img: file});
		console.log(cats);
		//console.log(JSON.stringify(cats));
		localStorage.setItem('cats', JSON.stringify(cats));
		alert("Готово");
		location.reload();
		return false;
	};
	function previewFile() {
	  var preview = document.querySelector('img');
	  var file1 = document.querySelector('input[type=file]').files[0];
	  var reader = new FileReader();

	  /*reader.onloadstart = function() {
	  	console.log("start");
	  }*/
	  reader.onloadend = function() {
	    preview.src = reader.result;
	    preview.height = '200';
	    //console.log("end");
	    file = reader.result;
	  }

	  if (file1) {
	    reader.readAsDataURL(file1);
	  } else {
	    preview.src = "";
	  }
	}

});